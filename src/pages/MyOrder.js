
import {useState, useEffect, useContext} from 'react'
import {Table, Button} from 'react-bootstrap'
import {Link, Navigate} from 'react-router-dom'
import MyOrderCard from './MyOrderCard'
import UserContext from '../UserContext'

export default function MyOrder() {

	// for authorization-----------------------------------------
	const {user} = useContext(UserContext)
	// console.log(user)

	

	// values sa loob ng table------------------------------------------
	/*const [productId, setProductId] = useState("")
	const [quantity, setQuantity] = useState("")
	const [totalCost, setTotalCost] = useState("")
	const [date, setDate] = useState("")*/
	const [orderList, setOrderList] = useState([])

	// para makuha data ng user------------------------------------------

	useEffect(() => {

		fetch('https://floating-atoll-72456.herokuapp.com/api/users/myOrders', {
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			// console.log(data)

			setOrderList(data.map(order => {

				return (
						<MyOrderCard key={order.productId} myOrderProp={order}/>
					)
			}))

		})
	},[])



	return (

			(user.id !== null) ?

				<>
					<h1 className="text-center m-4"><strong>My Order History</strong></h1>
					{orderList}
				</>
			:
				<Navigate to="*"/>

			

		)
}