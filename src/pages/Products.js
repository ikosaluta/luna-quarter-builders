import {Fragment, useEffect, useState} from 'react'
// import coursesData from '../data/coursesData'
import ProductCard from '../components/ProductCard'
// import './Products.css'

export default function Products () {

	// console.log(coursesData)
	// console.log(coursesData[0])

	const [products, setProducts] = useState([])

	useEffect(()=>{

		fetch('https://floating-atoll-72456.herokuapp.com/api/products/')
		.then(res => res.json())
		.then(data => {
			// console.log(data)

			setProducts(data.map(product => {

				return(
						<ProductCard key={product._id} productProp={product} />
					)
			}))
		})

	},[])


	/*const courses = coursesData.map(course => {
		return (
				key - para malaman na yung isang element is unique with other elements
				<CourseCard key={course.id} courseProp={course}/>
			)
	})*/

	return (

			
			<Fragment>
				<h1 className="text-center m-4"><strong>Products</strong></h1>
				{products}
			</Fragment>

		)
}