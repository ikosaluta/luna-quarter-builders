import {useState, useEffect} from 'react'
import {Card, Button} from 'react-bootstrap'
import {Link} from 'react-router-dom'
import './MyOrderCard.css'

export default function MyOrderCard({myOrderProp}) {


	const {productId, quantity, totalCost, purchasedOn} = myOrderProp
	console.log(myOrderProp)

	const [name, setName] = useState("")

	useEffect(()=>{
		// console.log(productId)

		fetch(`https://floating-atoll-72456.herokuapp.com/api/products/${productId}`)
		.then(res=> res.json())
		.then(data => {

			// console.log(data)

			setName(data.name)
		})

	},[productId])

	return (

				<>
					<div className="row justify-content-center">
					<div className="card m-2 p-3 col-md-6">
						<h6> <strong>Name:</strong> {name}</h6>
						<h6> <strong>productId:</strong> {productId}</h6>
						<h6> <strong>Quantity:</strong> {quantity}</h6>
						<h6> <strong>Total Amount:</strong> {totalCost}</h6>
						<h6> <strong>Date:</strong> {purchasedOn}</h6>
					</div>
					</div>
				</>
		
		)
}