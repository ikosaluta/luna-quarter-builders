import { useState, useEffect, useContext} from 'react'
import { Form, Button, Container } from 'react-bootstrap'
// navigate - redirect
import {Navigate, Link} from 'react-router-dom'
import Swal from 'sweetalert2'
import UserContext from '../UserContext'
import './Login.css'

export default function Login() {

	// Allows us to consume the User context object and its properties to use for our user validation
	const {user, setUser} = useContext(UserContext)

	// State hook to store the values of the input fields
	const [email, setEmail] = useState("")
	const [password, setPassword] = useState("")
	// to determine wether submit button is enabled or not
	const [isActive, setIsActive] = useState(false)


	// useEffect
	useEffect (() => {

		if(email !== "" && password !== "") {

			setIsActive(true)
		} else {
			setIsActive(false)
		}
	},[email, password])


	// login function
	function loginUser (e) {

		e.preventDefault()


		fetch('https://floating-atoll-72456.herokuapp.com/api/users/login', {
			method: 'POST',
			headers: {'Content-Type': 'application/json'},
			body: JSON.stringify({
				// manggagaling sa input fields
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {

			// console.log(data)
			
			if(typeof data.access !== "undefined") {

				localStorage.setItem('token', data.access)
				retrieveUserDetails(data.access)

				// pag nagsave na, dapat may lumabas na alert
				Swal.fire({
					title: "Log in Successful",
					icon: "success",
					text: "Welcome to LQBI Page"
				})
			} else {

				Swal.fire({
					title: "Authentication Failed",
					icon: "error",
					text: "Check your login details and try again"
				})
			}
		})




		// Set the email of the authenticated user in the local storage
		/*
			Syntax:
				localStorage.setItem("propertyName", value)
		*/
		// parang nagsesave siya ng item
		// localStorage.setItem("email", email)

		// set the global user state to have properties obtained from local storage
		/*setUser({
			email: localStorage.getItem('email')
		})*/

		// Clear input fields after submission
		setEmail("")
		setPassword("")

	}

	const retrieveUserDetails = (token) => {

		fetch('https://floating-atoll-72456.herokuapp.com/api/users/details', {

			headers: {
				Authorization: `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setUser({
				id: data._id,
				isAdmin: data.isAdmin,
				firstName: data.firstName,
				lastName: data.lastName,
				myOrders: data.myOrders
			})
		})

		console.log(user)
	}

	return(

		(user.id !== null) ?

			(user.isAdmin)?
				<Navigate to="/admin"/>

			:
			<Navigate to="/home" />
		:
			<div className="row justify-content-center">
			<div className="login-card p-4 m-4 col-md-6">
				<Form className="mt-3" onSubmit={(e) => loginUser(e)}>
					<h1 className="text-center">Log in</h1>
			      <Form.Group className="mb-3" controlId="userEmail">
			        <Form.Label className="form-label ">Email:</Form.Label>
			        <Form.Control 
			        	type="email" 
			        	placeholder="Enter email"
			        	value={email}
			        	onChange={e => {
			        		setEmail(e.target.value)
			        	}}
			        	required />
			        <Form.Text className="text-muted">
			          We'll never share your email with anyone else.
			        </Form.Text>
			      </Form.Group>

			      <Form.Group className="mb-3" controlId="password1">
			        <Form.Label className="form-label">Password</Form.Label>
			        <Form.Control 
			        	className="col-md-8 col-lg-6"
			        	type="password" 
			        	placeholder="Password"
			        	value={password}
			        	onChange={e => {setPassword(e.target.value)}}
			        	required />
			      </Form.Group>

			     {
			      	isActive ?
			      		<Button className="button1 mb-2"  type="submit" id="submitBtn">
					        Submit
					      </Button>
			      :
			      		<Button className="button2 mb-2"  type="submit" id="submitBtn" disabled>
					        Submit
					      </Button>
			      }
			      <p className="mb-5">Don't Have an account? <Link to="/register">Sign Up</Link></p>
			    </Form>
		    </div>
		    </div>

		)
}