import {useContext, useEffect} from 'react'
import {Navigate} from 'react-router-dom'
import UserContext from '../UserContext'

export default function	Logout () {

	// This allows us to consume the UserContext object and desctructure it to access the user state and unsetUser function from the context provider
	const {unsetUser, setUser} = useContext(UserContext)

	// Clear the local storage of the user's information
	unsetUser()

	useEffect(()=>{
		// set the user state back to its original value
		setUser({id: null})
	}, [])

	// localStorage.clear() - di na kailangan kasi may unsetUser na

	return (

			//Navigate - redirect agad sa link/path 
			<Navigate to="/" />
		)
}