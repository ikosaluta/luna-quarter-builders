import {Link} from 'react-router-dom'

export default function Error () {

	return(
		<>
			<h1 className="m-3">Page Not Found</h1>
			<div className="m-3">
				<span>Go back to the </span>
				<Link to="/">HomePage</Link>
			</div>
			
		</>
		)
}