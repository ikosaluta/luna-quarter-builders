import AdminBanner from './AdminBanner'
import AdminHighlights from './AdminHighlights'

export default function AdminHome () {

	return (
			<>
				<AdminBanner/>
				<AdminHighlights/>
			</>

		)
}