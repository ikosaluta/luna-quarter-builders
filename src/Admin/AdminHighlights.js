import {Row, Col, Card, Button} from 'react-bootstrap'
import {Link} from 'react-router-dom'
 
export default function AdminHighlights () {

	return (
			<Row className="mt-3 mb-3">
				<Col xs={12} md={6}>
					<Card className="cardHighLight p-3">			      
				      <Card.Body>
				        <Card.Title>Products</Card.Title>
				        <Card.Text>
				          You can <strong>ADD</strong>, <strong>EDIT</strong>, and <strong>ARCHIVE</strong> products at the products page.
				        </Card.Text>
				        <Button className="button" as={Link} to="/adminProducts">See all Products</Button>
				      </Card.Body>
				    </Card>
				</Col>

				<Col xs={12} md={6}>
					<Card className="cardHighLight p-3">				      
				      <Card.Body>
				        <Card.Title>All Orders</Card.Title>
				        <Card.Text>
				          You can see all orders from all users at the <strong>order List</strong> page.
				        </Card.Text>
				        <Button className="button">See all Orders</Button>
				      </Card.Body>
				    </Card>
				</Col>

			</Row>

		)
}