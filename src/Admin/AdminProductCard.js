import {useContext} from 'react'
import {Card, Button} from 'react-bootstrap'
import {Link} from 'react-router-dom'
import UserContext from	'../UserContext'
import './AdminProductCard.css'

export default function AdminProductCard({adminProductProp}) {

	const {name, description, price, _id, isActive, orders} = adminProductProp




	return (
			<div className="row justify-content-center">
				<div className="col-md-8 col-lg-6">
					<Card className="card m-4">
				      <Card.Body>
				        <Card.Title><strong>{name}</strong></Card.Title><br/>
				    {/*pwede din gamitin card.Title, card.Subtitle, card.Text*/}
				    	<Card.Subtitle><strong>Description:</strong></Card.Subtitle>
				        <Card.Text>{description}</Card.Text>
				        <Card.Subtitle><strong>Price:</strong></Card.Subtitle>
				        <Card.Text>Php {price}</Card.Text>
				        <Card.Subtitle><strong>isActive:</strong></Card.Subtitle>
				        <Card.Text>{isActive.toString()}</Card.Text>
				       

				        {
				        	(isActive == true) ?
				        		<>
				        		<Button  className="button d-inline m-1" as={Link} to={`/editProduct/${_id}`}>Edit</Button>

				        		<Button  className="button d-inline m-1" as={Link} to={`/adminOrderList/${_id}`}>Order List</Button>
				        		</>
				        	:
				        		<Button  className="button d-inline m-1" as={Link} to={`/editProduct/${_id}`}>Open</Button>
				        }
				      </Card.Body>
				    </Card>
				</div>
		    </div>
		)
}